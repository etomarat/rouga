const kTileSize = 1;

var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-display',
                           { preload: preload, create: create, update: update, render: render });

function preload()
{
    game.load.image('ground', '../img/dung1_2.png');
}

var map;
var layer;
var cursors;

function create()
{
    //  Creates a blank tilemap
    map = game.add.tilemap(null, 47, 45, 128, 128);

    //  Add a Tileset image to the map
    map.addTilesetImage('ground', 'ground', 47, 45, 0, 0, 0);

    //  Creates a new blank layer and sets the map dimensions.
    //  In this case the map is 128x128 tiles in size and the tiles are 47x45 pixels in size.
    layer = map.create('dungeon', 128, 128, 47, 45); //dungeon == level1
    layer.scrollFactorX = 2.0;
    layer.scrollFactorY = 2.0;

    //  Resize the world
    layer.resizeWorld();
    
    for (x = 0; x < 64; ++x)
    {
        for (y = 0; y < 64; ++y)
        {
            map.putTile(31, x, y, layer);
        }
    }
    
    cursors = game.input.keyboard.createCursorKeys();
}

function update() {

    if (cursors.left.isDown)
    {
        game.camera.x -= 4;
    }
    else if (cursors.right.isDown)
    {
        game.camera.x += 4;
    }

    if (cursors.up.isDown)
    {
        game.camera.y -= 4;
    }
    else if (cursors.down.isDown)
    {
        game.camera.y += 4;
    }

}

function render()
{
    game.debug.text('BSP Map Generator', 10, 20);
}

var canvas;
var context;

this.Point = (function()
{
    function Point(X, Y)
    {
        this.x = X;
        this.y = Y;
    }
    return Point;
})();

this.Rectangle = (function()
{
    function Rectangle(X, Y, Width, Height)
    {
        this.x = X;
        this.y = Y;
        this.width = Width;
        this.height = Height;
    }
    return Rectangle;
})();

this.Leaf = (function()
{
    function Leaf(X, Y, Width, Height, _kMinLeafSize)
    {
        this.MIN_LEAF_SIZE = _kMinLeafSize;
        this.x = X;
        this.y = Y;
        this.width = Width;
        this.height = Height;
        this.leftChild = null;
        this.rightChild = null;
        this.room = null;
        this.halls = [];
    }
    
    Leaf.prototype.splitLeaf = function()
    {
        console.log("Splitting leaf...");
        if (this.leftChild != null || this.rightChild != null)
        {
            console.log("Some child is NOT null!");
            return false;
        }
        var splitH = (Math.random() > 0.5);
        if (this.width > this.height && this.height / this.width >= 0.05)
        {
            splitH = false;
        }
        else if (this.height > this.width && this.width / this.height >=0.05)
        {
            splitH = true;
        }
        var max = (splitH ? this.height : this.width) - this.MIN_LEAF_SIZE;
        if (max < this.MIN_LEAF_SIZE)
        {
            console.log("max < this.MIN_LEAF_SIZE");
            return false;
        }
        var splitter = Math.round(this.MIN_LEAF_SIZE + Math.random() * (max - this.MIN_LEAF_SIZE));
        if (splitH)
        {
            this.leftChild = new Leaf(this.x, this.y, this.width, splitter, this.MIN_LEAF_SIZE);
            this.rightChild = new Leaf(this.x, this.y + splitter, this.width, this.height - splitter, this.MIN_LEAF_SIZE);
        }
        else
        {
            this.leftChild = new Leaf(this.x, this.y, splitter, this.height, this.MIN_LEAF_SIZE);
            this.rightChild = new Leaf(this.x + splitter, this.y, this.width - splitter, this.height, this.MIN_LEAF_SIZE);
        }
        console.log("All OK.");
        return true;
    };
    
    Leaf.prototype.createRooms = function(_kHallThickness)
    {
        if (this.leftChild != null || this.rightChild != null)
        {
            if (this.leftChild != null)
            {
                console.log("Checking leftChild...");
                this.leftChild.createRooms(_kHallThickness);
            }
            if (this.rightChild != null)
            {
                console.log("Checking rightChild...");
                this.rightChild.createRooms(_kHallThickness);
            }
            if (this.leftChild != null && this.rightChild != null)
            {
                console.log("Creating hall...");
                var l = this.leftChild.getRoom();
                var r = this.rightChild.getRoom();
                if (l != null && r != null)
                {
                    console.log("Inside createRooms:");
                    console.log("l:(%d, %d, %d, %d)", l.x, l.y, l.width, l.height);
                    console.log(l);
                    console.log("r:(%d, %d, %d, %d)", r.x, r.y, r.width, r.height);
                    console.log(r);
                    this.createHall(l, r, _kHallThickness);
                }
            }
        }
        else
        {
            var roomSize = new Point(3 + Math.random() * (this.width - 2 - 3),
                                     3 + Math.random() * (this.height - 2 -3));
            var roomPos = new Point(1 + Math.random() * (this.width - roomSize.x - 1 - 1),
                                    1 + Math.random() * (this.height - roomSize.y - 1 - 1));
            this.room = new Rectangle(this.x + roomPos.x, this.y + roomPos.y, roomSize.x, roomSize.y);
            console.log(this.room);
            if (this.room == null)
            {
                return false;
            }
            context.fillStyle = "#0000FF";
            context.fillRect(kTileSize * this.room.x, kTileSize * this.room.y,
                             kTileSize * this.room.width, kTileSize * this.room.height);
        }
        return true;
    }
    
    Leaf.prototype.getRoom = function()
    {
        if (this.room != null)
        {
            return this.room;
        }
        else
        {
            var lRoom, rRoom;
            if (this.leftChild != null)
            {
                lRoom = this.leftChild.getRoom();    
            }
            if (this.rightChild != null)
            {
                rRoom = this.rightChild.getRoom();
            }
            if (lRoom == null && rRoom == null)
            {
                return null;
            }
            else if (rRoom == null)
            {
                return lRoom;
            }
            else if (lRoom == null)
            {
                return rRoom;
            }
            else if (Math.random() > 0.5)
            {
                return lRoom;
            }
            else
            {
                return rRoom;
            }
        }
    }
    
    Leaf.prototype.createHall = function(l, r, _kHallThickness)
    {
        this.halls = [];
        
        var l_left = l.x;
        var l_top = l.y;
        var l_right = l.x + l.width;
        var l_bottom = l.y + l.height;

        var r_left = r.x;
        var r_top = r.y;
        var r_right = r.x + r.width;
        var r_bottom = r.y + r.height;
        
        var point1 = new Point(l_left + 1 + Math.random() * (l_right - 2 - (l_left + 1)),
                               l_top + 1 + Math.random() * (l_bottom - 2 - (l_top + 1)));
        var point2 = new Point(r_left + 1 + Math.random() * (r_right - 2 - (r_left + 1)),
                               r_top + 1 + Math.random() * (r_bottom - 2 - (r_top + 1)));
        
        console.log("Inside createHall:");
        console.log("l:(%d, %d, %d, %d)", l_left, l_top, l_right, l_bottom);
        console.log("r:(%d, %d, %d, %d)", r_left, r_top, r_right, r_bottom);
        
        console.log("point1.x = %d, point1.y = %d", point1.x, point1.y);
        console.log("point2.x = %d, point2.y = %d", point2.x, point2.y);
        
        var w = point2.x - point1.x;
        var h = point2.y - point1.y;
        
        console.log("w = %d, h = %d", w, h);
        
        var thickness = _kHallThickness;
        var color = "#0000FF";
        if (w < 0)
        {
            if (h < 0)
            {
                if (Math.random() * 0.5)
                {    
                    this.halls.push(new Rectangle(point2.x, point1.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point2.x, point2.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point2.x, kTileSize * point1.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point2.x, kTileSize * point2.y, kTileSize * thickness, kTileSize * Math.abs(h));
                }
                else
                {
                    this.halls.push(new Rectangle(point2.x, point2.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point1.x, point2.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point2.x, kTileSize * point2.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point1.x, kTileSize * point2.y, kTileSize * thickness, kTileSize * Math.abs(h));
                }
            }
            else if (h > 0)
            {
                if (Math.random() * 0.5)
                {
                    this.halls.push(new Rectangle(point2.x, point1.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point2.x, point1.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point2.x, kTileSize * point1.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point2.x, kTileSize * point1.y, kTileSize * thickness, Math.abs(h));
                }
                else
                {
                    this.halls.push(new Rectangle(point2.x, point2.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point1.x, point1.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point2.x, kTileSize * point2.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point1.x, kTileSize * point1.y, kTileSize * thickness, kTileSize * Math.abs(h));
                }
            }
            else //if (h == 0)
            {
                this.halls.push(new Rectangle(point2.x, point2.y, Math.abs(w), thickness));
                context.fillStyle = color;
                context.fillRect(kTileSize * point2.x, kTileSize * point2.y, kTileSize * Math.abs(w), kTileSize * thickness);
            }
        }
        else if (w > 0)
        {
            if (h < 0)
            {
                if (Math.random() * 0.5)
                {
                    this.halls.push(new Rectangle(point1.x, point2.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point1.x, point2.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point1.x, kTileSize * point2.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point1.x, kTileSize * point2.y, kTileSize * thickness, kTileSize * Math.abs(h));
                }
                else
                {
                    this.halls.push(new Rectangle(point1.x, point1.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point2.x, point2.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point1.x, kTileSize * point1.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point2.x, kTileSize * point2.y, kTileSize * thickness, kTileSize * Math.abs(h));
                }
            }
            else if (h > 0)
            {
                if (Math.random() * 0.5)
                {
                    this.halls.push(new Rectangle(point1.x, point1.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point2.x, point1.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point1.x, kTileSize * point1.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point2.x, kTileSize * point1.y, kTileSize * thickness, kTileSize * Math.abs(h));
                }
                else
                {
                    this.halls.push(new Rectangle(point1.x, point2.y, Math.abs(w), thickness));
                    this.halls.push(new Rectangle(point1.x, point1.y, thickness, Math.abs(h)));
                    context.fillStyle = color;
                    context.fillRect(kTileSize * point1.x, kTileSize * point2.y, kTileSize * Math.abs(w), kTileSize * thickness);
                    context.fillRect(kTileSize * point1.x, kTileSize * point1.y, kTileSize * thickness, kTileSize * Math.abs(h));
                }
            }
            else // if (h == 0)
            {
                this.halls.push(new Rectangle(point1.x, point1.y, Math.abs(w), thickness));
                context.fillStyle = color;
                context.fillRect(kTileSize * point1.x, kTileSize * point1.y, kTileSize * Math.abs(w), kTileSize * thickness);
            }
        }
        else // if (w == 0)
        {
            if (h < 0)
            {
                this.halls.push(new Rectangle(point2.x, point2.y, thickness, Math.abs(h)));
                context.fillStyle = color;
                context.fillRect(kTileSize * point2.x, kTileSize * point2.y, kTileSize * thickness, kTileSize * Math.abs(h));
            }
            else if (h > 0)
            {
                this.halls.push(new Rectangle(point1.x, point1.y, thickness, Math.abs(h)));
                context.fillStyle = color;
                context.fillRect(kTileSize * point1.x, kTileSize * point1.y, kTileSize * thickness, kTileSize * Math.abs(h));
            }
        }
    }
    
    return Leaf;
})();

function generateBSPMain(argc, argv, _kGridWidth, _kGridHeight, _kMinLeafSize, _kMaxLeafSize, _kHallThickness, _showLeafs)
{
    var leafs = [];
    var leaf;

    canvas = document.getElementById("display");
    context = canvas.getContext("2d");
    
    context.clearRect(0, 0, 800, 600);

    var root = new Leaf(0, 0, _kGridWidth, _kGridHeight, _kMinLeafSize);
    console.log(root);
    leafs.push(root);

    var did_split = true;
    while (did_split)
    {
        did_split = false;
        for (i = 0; i < leafs.length; ++i)
        {
            console.log(leafs[i]);
            if (leafs[i].leftChild == null && leafs[i].rightChild == null)
            {
                console.log("%d - %d && %d - %d", leafs[i].width, _kMaxLeafSize, leafs[i].height, _kMaxLeafSize);
                if (leafs[i].width > _kMaxLeafSize || leafs[i].height > _kMaxLeafSize)
                {
                    console.log("Leaf needs splitting...");
                    var result = leafs[i].splitLeaf();
                    if (result == true)
                    {
                        leafs.push(leafs[i].leftChild);
                        leafs.push(leafs[i].rightChild);
                        did_split = true;
                        console.log("splitLeaf() success:");
                        console.log(leafs[i].leftChild);
                        console.log(leafs[i].rightChild);
                    }
                    else
                    {
                        console.log("splitLeaf() fail:");
                        console.log(result);
                        //return -1;
                    }
                }
            }
        }
    }

    console.log(leafs.length);
    if (_showLeafs == true)
    {
        for (i = 0; i < leafs.length; ++i)
        {
            context.strokeStyle = "#AAAAAA";
            context.strokeRect(leafs[i].x, leafs[i].y, leafs[i].width, leafs[i].height);
        }
    }
    
    var result = root.createRooms(_kHallThickness);
    if (result == true)
    {
        console.log("Rooms created successfully");
    }
    else
    {
        console.log("Error creating rooms!");
        return -1;
    }
    console.log(root);
    
    return 0;
}

const kGridWidth = 512;
const kGridHeight = 512;
const kMinLeafSize = 10;
const kMaxLeafSize = 50;
const kHallThickness = 1;

function showGeneratedMapOnly()
{
    generateBSPMain(1, "bsp", kGridWidth, kGridHeight, kMinLeafSize, kMaxLeafSize, kHallThickness, false);    
}

function showGeneratedMapWithSplits()
{
    generateBSPMain(1, "bsp", kGridWidth, kGridHeight, kMinLeafSize, kMaxLeafSize, kHallThickness, true);    
}
