var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'game-canvas', { preload: preload, create: create, update: update, render: render });

function preload() {
    game.load.image('chest', 'img/chest.png');
    game.load.image('dead-spider', 'img/dead-spider.png');
    game.load.image('ground_1x1', 'img/dung1_4.png');
    game.load.image('spider', 'img/spider.png');
    game.load.image('sword', 'img/sword.png', 69, 18, 1);
    game.load.spritesheet('hp', 'img/hp.png', 48, 45, 4);
    game.load.spritesheet('player-move', 'img/Boy.png', 45, 45, 12);
    game.load.spritesheet('slash', 'img/slash.png', 60, 39, 2);
    game.load.spritesheet('spider-move', 'img/spider3.png', 45, 45, 12);
    game.load.spritesheet('walking_dead', 'img/walking.png', 75, 56, 3);
    game.load.tilemap('dungeon', 'json/dung1_4.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.audio('silius', ['audio/Naoki_Kodaka_Journey_to_Silius_Title_Theme_44KHz.ogg', 'audio/Journey_to_Silius_Stage_3_44KHz.ogg']);
};

var map;
var layer;

var tileSize = {
    x: 45,
    y: 45
};

_map = [];
for (var i=0; i<44; i++) {
    _map.push(i)
};
var walls = _map.slice(0, 11).concat(_map.slice(13, 22), _map.slice(29, 33), _map.slice(35, 44))
var doors = [11, 12]

var playerStepSpeed = 100;
var enemyStepSpeed = 400;

item = {
    title:'',
    type:'',
    img:'',
    cost:0,
    stats:{
        hp:0,
        str:0,
        dex:0,
    }
}

unitInfo = {
        maxhp:10,
        hp:10,
        str:10,
        dex:10,
        inv:[],
        gold:0,
        invMax: 5,
        equip:{
            head:null,
            body:null,
            foot:null,
            rhend:null,
            lhend:null
        },
        addToInv: function(item) {
            if (this.inv.length < this.invMax) {
                this.inv.push(item)
                return true
            } else {
                alert('В инвентаре нет места!')
                this.dropItem(item);
                return false
            }
        },
        dropItem: function(item){
            this.removeFromInv(item);
            alert('Предмет вероятно сброшен на землю')
        },
        removeFromInv: function(item){
            if ( in_array(item, this.inv) ) {
                this.inv.splice(this.inv.indexOf(item), 1);
                return true
            } else {
                return false
            }
        },
        isWeare: function(item) {
            if (item.type in this.equip) {
                return true
            } else {
                return false
            }
        },
        addToEquip: function(item) {
            if (this.isWeare(item)) {
                _c = this.equip[item.type];
                this.equip[item.type]=item;
                if (_c) {
                    this.addToInv(_c);
                }
                return true
            } else {
                alert('Этот предмет нельзя носить');
                return false
            }
        },
        removeFromEquip: function(item){
            if (this.isWeare(item) && this.equip[item.type]==item) {
                this.addToInv(item);
                return true
            } else {
                return false
            }
        }
    }

function in_array(value, array) {
    for(var i = 0; i < array.length; i++) {
        if(array[i] == value) return true;
    };
    return false;
};


var marker;
var player;

var gui;

var target = {
    x: NaN,
    y: NaN,
    len: 0
}

var keyPress = false;

var tileMap = [];
var tileProperty = {
    // 0x0 - 0x9 - препятсятвия:
    WALL: 0x0,
    ENEMY: 0x1,

    // 0xa - 0xf - проходы:
    FLOOR: 0xa,
    DOOR: 0xb,

    PLAYER: 0xf
}

function create() {
    music = game.add.audio('silius');
    music.loop = true;
    music.play();

    game.stage.backgroundColor = '#2d2d2d';

    map = game.add.tilemap('dungeon');
    
    for (var x=0; x<map.width; x++) {
        tileMap[x] = [];
        for (var y=0; y<map.height; y++){
            var tileType = map.getTile(x, y);
            if (tileType === null) {
                continue
            } else {
                tileType = tileType.index
            }
            if (in_array(tileType, walls)) {
                tileMap[x][y] = tileProperty.WALL;
            } else if (in_array(tileType, doors)) {
                tileMap[x][y] = tileProperty.DOOR;
            } else {
                tileMap[x][y] = tileProperty.FLOOR;
            }
        }
    }

    game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
    map.addTilesetImage('ground_1x1');
    layer = map.createLayer(0);

    // layer = map.create('level1', 40, 30, tileSize.x, tileSize.y);

    layer.resizeWorld();

    enemy = game.add.sprite(tileSize.x * 17, tileSize.y * 10, 'spider-move');

    chest = game.add.sprite(tileSize.x * 9, tileSize.y * 28, 'chest');
    walking_dead = game.add.sprite(tileSize.x * 9, tileSize.y * 26, 'walking_dead');

    player = game.add.sprite(tileSize.x * 4, tileSize.y * 3, 'player-move');

    player.oldCoord = {
        x: player.x,
        y: player.y
    };
    player.direction = 'right'

    player.oldTileProperty = tileMap[layer.getTileX(player.x)][layer.getTileY(player.y)]
    tileMap[layer.getTileX(player.x)][layer.getTileY(player.y)] = tileProperty.PLAYER;

    player.animations.add('walk-down', [0, 1]);
    player.animations.add('walk-up', [3, 4]);
    player.animations.add('walk-right', [6, 7]);
    player.animations.add('walk-left', [9, 10]);

    player.animations.add('stay-down', [5]);
    player.animations.add('stay-up', [2]);
    player.animations.add('stay-right', [8]);
    player.animations.add('stay-left', [11]);

    player.loadTexture('slash');
    player.animations.add('slash', [0, 1, 0]);
    // player.loadTexture('slash');
    // player.animations.add('slash-left', [2, 3, 2]);

    player.animations.play('stay-' + player.direction, 1, false)
    player.unitInfo = unitInfo
    
    walking_dead.animations.add('swirl', [0, 2]);
    walking_dead.animations.play('swirl', 5, true)


    enemy.oldCoord = {
        x: enemy.x,
        y: enemy.y
    };

    enemy.oldTileProperty = tileMap[layer.getTileX(enemy.x)][layer.getTileY(enemy.y)]
    tileMap[layer.getTileX(enemy.x)][layer.getTileY(enemy.y)] = tileProperty.ENEMY;
    
    enemy.animations.add('walk-down', [4, 5]);
    enemy.animations.add('walk-up', [10, 11]);
    enemy.animations.add('walk-right', [8, 9]);
    enemy.animations.add('walk-left', [6, 7]);

    enemy.animations.add('stay-down', [0]);
    enemy.animations.add('stay-up', [1]);
    enemy.animations.add('stay-right', [2]);
    enemy.animations.add('stay-left', [3]);

    enemy.direction = 'down'
    enemy.animations.play('stay-' + enemy.direction, 1, false)
    
    enemy.hp = 5
    enemy.dead = false

    game.physics.enable(player);
    game.physics.enable(enemy);

    game.camera.follow(player);
    game.isMove = false;
    game.AIStep = false;


    marker = game.add.graphics();
    marker.lineStyle(2, 0x000000, 1);
    marker.drawRect(0, 0, tileSize.x, tileSize.y);

    cursors = game.input.keyboard.createCursorKeys();

    game.input.setMoveCallback(updateMarker, this);
    
    gui = game.add.group();
    gui['hp'] = game.add.sprite(50, 30, 'hp', 0, gui)
    gui['sword'] = game.add.sprite(700, 30, 'sword', 0, gui)
    gui.fixedToCamera = true;
    
    fullscreen = game.input.keyboard.addKey(Phaser.Keyboard.F);
    fullscreen.onDown.add(toFoolScreen, this);    
};


function updateMarker() {
    marker.x = layer.getTileX(game.input.activePointer.worldX) * tileSize.x;
    marker.y = layer.getTileY(game.input.activePointer.worldY) * tileSize.y;

    if (game.input.mousePointer.isDown) {
        target.x = layer.getTileX(game.input.activePointer.worldX) * tileSize.x;
        target.y = layer.getTileY(game.input.activePointer.worldY) * tileSize.y;
    }
};

function len(unit) {
    return Math.sqrt(Math.pow((unit.x - player.x), 2) + Math.pow((unit.y - player.y), 2))
};

function hitEnemy(enemy_) {
    player.animations.play('slash', 15, false);
    if (enemy.hp<=0) {
        tileMap[layer.getTileX(enemy.oldCoord.x)][layer.getTileY(enemy.oldCoord.y)] = enemy.oldTileProperty;
        enemy.dead = true;
        enemy.animations.stop();
        enemy.loadTexture('dead-spider');
        // enemy.animations.add('dead');
        // enemy.animations.play('dead', 10, true);
    } else {
        enemy.hp--
    }
}

function update() {

    if (!(target.x!=target.x && target.y!=target.y)) {
        if (!game.isMove) {
            if (Math.abs(target.x-player.x) > Math.abs(target.y-player.y)) {
                if (target.x > player.x) {
                    next = tileMap[layer.getTileX(player.oldCoord.x) + 1][layer.getTileY(player.oldCoord.y)]
                    if (next >= tileProperty.FLOOR) {
                        player.direction = 'right';
                        player.animations.play('walk-' + player.direction, 4, true);
                        player.body.velocity.x = playerStepSpeed;
                        game.isMove = true;
                    } else {
                        if (next === tileProperty.ENEMY) {
                            console.log('BOOM!!!1');
                            hitEnemy(enemy)
                            game.AIStep = true;
                        }
                        target.x = NaN;
                        target.y = NaN;
                    }
                } else {
                    next = tileMap[layer.getTileX(player.oldCoord.x) - 1][layer.getTileY(player.oldCoord.y)]
                    if (next >= tileProperty.FLOOR) {
                        player.direction = 'left';
                        player.animations.play('walk-' + player.direction, 4, true);
                        player.body.velocity.x = -playerStepSpeed;
                        game.isMove = true;
                    } else {
                        if (next === tileProperty.ENEMY) {
                            console.log('BOOM!!!1');
                            hitEnemy(enemy)
                            game.AIStep = true;
                        }
                        target.x = NaN;
                        target.y = NaN;
                    }
                };
                player.y = player.oldCoord.y;
            } else {
                if (target.y > player.y) {
                    next = tileMap[layer.getTileX(player.oldCoord.x)][layer.getTileY(player.oldCoord.y) + 1]
                    if (next >= tileProperty.FLOOR) {
                        player.direction = 'down';
                        player.animations.play('walk-' + player.direction, 4, true);
                        player.body.velocity.y = playerStepSpeed;
                        game.isMove = true;
                    } else {
                        if (next === tileProperty.ENEMY) {
                            console.log('BOOM!!!1');
                            hitEnemy(enemy)
                            game.AIStep = true;
                        }
                        target.x = NaN;
                        target.y = NaN;
                    }
                } else {
                    next = tileMap[layer.getTileX(player.oldCoord.x)][layer.getTileY(player.oldCoord.y - 1)]
                    if (next >= tileProperty.FLOOR) {
                        player.direction = 'up';
                        player.animations.play('walk-' + player.direction, 4, true);
                        player.body.velocity.y = -playerStepSpeed;
                        game.isMove = true;
                    } else {
                        if (next === tileProperty.ENEMY) {
                            console.log('BOOM!!!1');
                            hitEnemy(enemy)
                            game.AIStep = true;
                        }
                        target.x = NaN;
                        target.y = NaN;
                    }
                };
                player.x = player.oldCoord.x;
            }
        }
    } else {
        player.y = player.oldCoord.y;
        player.x = player.oldCoord.x;
    };

    if (Math.abs(player.x - player.oldCoord.x)>tileSize.x) {
        player.body.velocity.x = 0;
        player.x = layer.getTileX(player.oldCoord.x) * tileSize.x + tileSize.x * Math.abs(player.x - player.oldCoord.x)/(player.x - player.oldCoord.x);

        tileMap[layer.getTileX(player.oldCoord.x)][layer.getTileY(player.oldCoord.y)] = player.oldTileProperty;
        player.oldTileProperty = tileMap[layer.getTileX(player.x)][layer.getTileY(player.y)]
        tileMap[layer.getTileX(player.x)][layer.getTileY(player.y)] = tileProperty.PLAYER;

        player.oldCoord.x = player.x;
        game.isMove = false;
        game.AIStep = true;
        player.animations.play('stay-' + player.direction, 1, false);
    } else if (Math.abs(player.y - player.oldCoord.y)>tileSize.y) {
        player.body.velocity.y = 0;
        player.y = layer.getTileY(player.oldCoord.y) * tileSize.y + tileSize.y * Math.abs(player.y - player.oldCoord.y)/(player.y - player.oldCoord.y);

        tileMap[layer.getTileX(player.oldCoord.x)][layer.getTileY(player.oldCoord.y)] = player.oldTileProperty;
        player.oldTileProperty = tileMap[layer.getTileX(player.x)][layer.getTileY(player.y)]
        tileMap[layer.getTileX(player.x)][layer.getTileY(player.y)] = tileProperty.PLAYER;

        player.oldCoord.y = player.y;
        game.isMove = false;
        game.AIStep = true;
        player.animations.play('stay-' + player.direction, 1, false);
    };

    if (player.x==target.x && player.y==target.y) {
        target.x = NaN;
        target.y = NaN;
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;
        game.isMove = false;
    }

    // game.AIStep = false;
    function playerHit() {
        player.unitInfo.hp--;
    }
    if (game.AIStep && !enemy.dead) {
        console.log('enemy')
        if (!game.isMove) {
            if (300 > len(enemy)) {
                if (Math.abs(enemy.x-player.x) > Math.abs(enemy.y-player.y)) {
                    if (player.x > enemy.x) {
                        next = tileMap[layer.getTileX(enemy.oldCoord.x) + 1][layer.getTileY(enemy.oldCoord.y)]
                        if (next >= tileProperty.FLOOR && next < tileProperty.PLAYER) {
                            enemy.direction = 'right';
                            enemy.animations.play('walk-' + enemy.direction, 4, true);
                            enemy.body.velocity.x = enemyStepSpeed;
                            game.isMove = true;
                        } else if (next === tileProperty.PLAYER) {
                            console.log('Игрок получает по щам!');
                            game.AIStep = false;
                        }
                    } else {
                        next = tileMap[layer.getTileX(enemy.oldCoord.x) - 1][layer.getTileY(enemy.oldCoord.y)]
                        if (next >= tileProperty.FLOOR && next < tileProperty.PLAYER) {
                            enemy.direction = 'left';
                            enemy.animations.play('walk-' + enemy.direction, 4, true);
                            enemy.body.velocity.x = -enemyStepSpeed;
                            game.isMove = true;
                        } else if (next === tileProperty.PLAYER) {
                            console.log('Игрок получает по щам!');
                            playerHit();
                            game.AIStep = false;
                        }
                    };
                    enemy.y = enemy.oldCoord.y;
                } else {
                    if (player.y > enemy.y) {
                        next = tileMap[layer.getTileX(enemy.oldCoord.x)][layer.getTileY(enemy.oldCoord.y) + 1]
                        if (next >= tileProperty.FLOOR && next < tileProperty.PLAYER) {
                            enemy.direction = 'down';
                            enemy.animations.play('walk-' + enemy.direction, 4, true);
                            enemy.body.velocity.y = enemyStepSpeed;
                            game.isMove = true;
                        } else if (next === tileProperty.PLAYER) {
                            console.log('Игрок получает по щам!');
                            playerHit();
                            game.AIStep = false;
                        }
                    } else {
                        next = tileMap[layer.getTileX(enemy.oldCoord.x)][layer.getTileY(enemy.oldCoord.y - 1)]
                        if (next >= tileProperty.FLOOR && next < tileProperty.PLAYER) {
                            enemy.direction = 'up';
                            enemy.animations.play('walk-' + enemy.direction, 4, true);
                            enemy.body.velocity.y = -enemyStepSpeed;
                            game.isMove = true;
                        } else if (next === tileProperty.PLAYER) {
                            console.log('Игрок получает по щам!');
                            playerHit();
                            game.AIStep = false;
                        }
                    };
                    enemy.x = enemy.oldCoord.x;
                }
            } else {
                game.AIStep = false;
            }
        } else {
            if (Math.abs(enemy.x - enemy.oldCoord.x)>tileSize.x) {
                enemy.body.velocity.x = 0;
                enemy.x = layer.getTileX(enemy.oldCoord.x) * tileSize.x + tileSize.x * Math.abs(enemy.x - enemy.oldCoord.x)/(enemy.x - enemy.oldCoord.x);

                tileMap[layer.getTileX(enemy.oldCoord.x)][layer.getTileY(enemy.oldCoord.y)] = enemy.oldTileProperty;
                enemy.oldTileProperty = tileMap[layer.getTileX(enemy.x)][layer.getTileY(enemy.y)]
                tileMap[layer.getTileX(enemy.x)][layer.getTileY(enemy.y)] = tileProperty.ENEMY;

                enemy.oldCoord.x = enemy.x;
                game.isMove = false;
                game.AIStep = false;
            } else if (Math.abs(enemy.y - enemy.oldCoord.y)>tileSize.y) {
                enemy.body.velocity.y = 0;
                enemy.y = layer.getTileY(enemy.oldCoord.y) * tileSize.y + tileSize.y * Math.abs(enemy.y - enemy.oldCoord.y)/(enemy.y - enemy.oldCoord.y);

                tileMap[layer.getTileX(enemy.oldCoord.x)][layer.getTileY(enemy.oldCoord.y)] = enemy.oldTileProperty;
                enemy.oldTileProperty = tileMap[layer.getTileX(enemy.x)][layer.getTileY(enemy.y)]
                tileMap[layer.getTileX(enemy.x)][layer.getTileY(enemy.y)] = tileProperty.ENEMY;

                enemy.oldCoord.y = enemy.y;
                game.isMove = false;
                game.AIStep = false;
            }
        }
    } else {
        enemy.x = enemy.oldCoord.x;
        enemy.y = enemy.oldCoord.y;
    }

    // Пропуск хода - стрелка влево
    // if (cursors.left.isDown) {
    //     game.AIStep = !keyPress;
    //     keyPress = true;
    // } else if (cursors.left.isUp) {
    //     keyPress = false;
    // }

}

function render() {

    // game.debug.text(player.x, 16, 350);
    // game.debug.text(player.y, 16, 370);
    // game.debug.text(enemy.x, 260, 350);
    // game.debug.text(enemy.y, 260, 370);
    // game.debug.text(len(enemy), 16, 590);
    cur_hp = 4 - Math.round(player.unitInfo.hp/(player.unitInfo.maxhp/4));
     if (gui.hp.frame != cur_hp) {
        gui.hp.frame = cur_hp
     }
}

function toFoolScreen () {
    game.scale.startFullScreen();
}
