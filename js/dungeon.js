var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser',
                           { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('theboy', 'img/TheBoy.png');
    game.load.image('spider', 'img/spider.png');
    //tilemap(key, mapDataURL, mapData, format) -> {Phaser.Loader}
    game.load.tilemap('dungeon', 'json/dung1_2.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('ground', 'img/dung1_2.png');
    game.load.audio('silius', ['audio/Naoki_Kodaka_Journey_to_Silius_Title_Theme_44KHz.ogg', 'audio/Journey_to_Silius_Stage_3_44KHz.ogg']);
}

var map;
var tileset;
var layer;
var sprite;
var group;
var oldY = 0;
var cursors;
var locs = [];
var music;

function create() {

    //  Create our tilemap to walk around
    map = game.add.tilemap('dungeon');
    map.addTilesetImage('ground');
    layer = map.createLayer('dungeon');

    //  This group will hold the main player + all the tree sprites to depth sort against
    group = game.add.group();
    sprite = group.create(150, 90, 'theboy');
    group.create(300, 90, 'spider');
    group.sort();

    //Sound it
    music = game.add.audio('silius');
    music.loop = true;
    music.play();

    //  Move it
    cursors = game.input.keyboard.createCursorKeys();
}

function createUniqueLocation() {

    do
    {
        var x = game.math.snapTo(game.world.randomX, 32) / 32;
        var y = game.math.snapTo(game.world.randomY, 32) / 32;
        if (y > 17)
        {
            y = 17;
        }
        var idx = (y * 17) + x;
    }
    while (locs.indexOf(idx) !== -1)
    locs.push(idx);
    group.create(x * 32, y * 32, 'trees', game.rnd.integerInRange(0, 7));
}

function update() {

    if (cursors.left.isDown)
    {
        sprite.x -= 2;
    }
    else if (cursors.right.isDown)
    {
        sprite.x += 2;
    }
    if (cursors.up.isDown)
    {
        sprite.y -= 2;
    }
    else if (cursors.down.isDown)
    {
        sprite.y += 2;
    }
    group.sort('y', Phaser.Group.SORT_ASCENDING);
}

function render()
{
    game.debug.text('Dungeon demo', 10, 20);
    //game.debug.soundInfo(music, 10, 32);
}