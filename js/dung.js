var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('phaser', 'img/phaser-dude.png');
    game.load.tilemap('dungeon', 'json/dung1_3.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('ground_1x1', 'img/dung1_3.png');
    game.load.spritesheet('hp', 'img/hp.png', 48, 45, 4);
    //game.load.spritesheet('trees', 'img/walls_1x2.png', 32, 64);
    

}

var map;
var tileset;
var layer;

var player;
var group;
var gui;
var oldY = 0;
var cursors;
var locs = [];

_map = []
for (var i=0; i< 44; i++) {
    _map.push(i)
}
var walls = _map.slice(0, 11).concat(_map.slice(13, 22), _map.slice(30, 34), _map.slice(36, 44))
var doors = [11, 12]

item = {
    title:'',
    type:'',
    img:'',
    cost:0,
    stats:{
        hp:0,
        str:0,
        dex:0,
    }
}

unitInfo = {
        maxhp:10,
        hp:10,
        str:10,
        dex:10,
        inv:[],
        gold:0,
        invMax: 5,
        equip:{
            head:null,
            body:null,
            foot:null,
            rhend:null,
            lhend:null
        },
        addToInv: function(item) {
            if (this.inv.length < this.invMax) {
                this.inv.push(item)
                return true
            } else {
                alert('В инвентаре нет места!')
                this.dropItem(item);
                return false
            }
        },
        dropItem: function(item){
            this.removeFromInv(item);
            alert('Предмет вероятно сброшен на землю')
        },
        removeFromInv: function(item){
            if ( in_array(item, this.inv) ) {
                this.inv.splice(this.inv.indexOf(item), 1);
                return true
            } else {
                return false
            }
        },
        isWeare: function(item) {
            if (item.type in this.equip) {
                return true
            } else {
                return false
            }
        },
        addToEquip: function(item) {
            if (this.isWeare(item)) {
                _c = this.equip[item.type];
                this.equip[item.type]=item;
                if (_c) {
                    this.addToInv(_c);
                }
                return true
            } else {
                alert('Этот предмет нельзя носить');
                return false
            }
        },
        removeFromEquip: function(item){
            if (this.isWeare(item) && this.equip[item.type]==item) {
                this.addToInv(item);
                return true
            } else {
                return false
            }
        }
    }

function in_array(value, array) 
{
    for(var i = 0; i < array.length; i++) 
    {
        if(array[i] == value) return true;
    }
    return false;
}

function create() {

    //  Create our tilemap to walk around
    map = game.add.tilemap('dungeon');
    map.addTilesetImage('ground_1x1');
    layer = map.createLayer(0);
    group = game.add.group();
    
    player = game.add.sprite(300, 28, 'phaser');
    
    player.unitInfo = unitInfo

    gui = game.add.group();
    gui['hp'] = game.add.sprite(50, 520, 'hp', 0, gui)
    gui.fixedToCamera = true;
    //  Move it
    cursors = game.input.keyboard.createCursorKeys();
    layer.resizeWorld();
    
    game.camera.follow(player);
    game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
    
    fullscreen = game.input.keyboard.addKey(Phaser.Keyboard.F);
    fullscreen.onDown.add(toFoolScreen, this);
}

function update() {

    if (cursors.left.isDown)
    {
        player.x -= 2;
    }
    else if (cursors.right.isDown)
    {
        player.x += 2;
    }

    if (cursors.up.isDown)
    {
        player.y -= 2;
    }
    else if (cursors.down.isDown)
    {
        player.y += 2;
    }

    
    
}

function render() {

    game.debug.text('hp: ' + player.unitInfo.hp, 10, 20);
    
    game.debug.text('hp: ' + i, 10, 40);
    
    cur_hp = 4 - Math.round(player.unitInfo.hp/(player.unitInfo.maxhp/4));
    if (gui.hp.frame != cur_hp) {
        gui.hp.frame = cur_hp
    }
}

function toFoolScreen () {
    game.scale.startFullScreen();
}