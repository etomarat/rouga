var __bind = function(fn, me)
{
  return function()
  {
    return fn.apply(me, arguments);
  };
};

this.Croissant = {
  Generators: {},
  Terrains: {}
};

this.Croissant.Level = (function()
{
  function Level(size)
  {
    var col, row, _i, _j, _ref, _ref1;
    this.tiles = [];
    if (size[0] && size[1])
    {
      this.rows = size[0];
      this.columns = size[1];
    }
    else
    {
      this.rows = size[0] ? size[0] : size;
      this.columns = this.rows;
    }
    for (row = _i = 0, _ref = this.rows - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; row = 0 <= _ref ? ++_i : --_i)
    {
      this.tiles.push([]);
      for (col = _j = 0, _ref1 = this.columns - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; col = 0 <= _ref1 ? ++_j : --_j)
      {
        this.tiles[row][col] = new Croissant.MapTile('wall');
      }
    }
  }

  return Level;
})();

this.Croissant.MapTile = (function()
{
  function MapTile(terrain)
  {
    this.terrain = Croissant.Terrains[terrain];
  }

  return MapTile;
})();

this.Croissant.Terrain = (function()
{
  function Terrain(name, blocking)
  {
    this.name = name;
    this.blocking = blocking;
    Croissant.Terrains[name] = this;
  }

  return Terrain;
})();

this.Croissant.Canvas = (function()
{
  function Canvas(element, name, stack)
  {
    this.clear = __bind(this.clear, this);
    this.draw_boundaries = __bind(this.draw_boundaries, this);
    this.draw_grid = __bind(this.draw_grid, this);
    this.name = name;
    this.element = element;
    this.stack = stack;
    this.gridsize = stack.gridsize;
    this.size_x = stack.size_x;
    this.size_y = stack.size_y;
    this.context = element.getContext('2d');
  }

  Canvas.prototype.draw_grid = function(color)
  {
    var pos_x, pos_y, prev_stroke_style, _i, _j, _ref, _ref1;
    if (color == null) {
      color = null;
    }
    prev_stroke_style = this.context.strokeStyle;
    if (color)
    {
      this.context.strokeStyle = color;
    }
    for (pos_x = _i = 0, _ref = this.size_x - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; pos_x = 0 <= _ref ? ++_i : --_i)
    {
      for (pos_y = _j = 0, _ref1 = this.size_y - 1; 0 <= _ref1 ? _j <= _ref1 : _j >= _ref1; pos_y = 0 <= _ref1 ? ++_j : --_j)
      {
        this.context.strokeRect(pos_x * this.gridsize, pos_y * this.gridsize, this.gridsize, this.gridsize);
      }
    }
    return this.context.strokeStyle = prev_stroke_style;
  };

  Canvas.prototype.draw_boundaries = function(boundary_array, color)
  {
    var normalized_height, normalized_width, normalized_x, normalized_y, prev_stroke_style, room, _i, _len, _ref;
    if (color == null)
    {
      color = null;
    }
    prev_stroke_style = this.context.strokeStyle;
    if (color)
    {
      this.context.strokeStyle = color;
    }
    _ref = boundary_array.rooms;
    for (_i = 0, _len = _ref.length; _i < _len; _i++)
    {
      room = _ref[_i];
      normalized_x = Math.floor(room.x[0] * this.stack.canvas_width / boundary_array.level.columns);
      normalized_width = Math.floor(room.x[1] * this.stack.canvas_width / boundary_array.level.columns) - normalized_x;
      normalized_y = Math.floor(room.y[0] * this.stack.canvas_height / boundary_array.level.rows);
      normalized_height = Math.floor(room.y[1] * this.stack.canvas_height / boundary_array.level.rows) - normalized_y;
      this.context.strokeRect(normalized_x, normalized_y, normalized_width, normalized_height);
    }
    return this.context.strokeStyle = prev_stroke_style;
  };

  Canvas.prototype.clear = function()
  {
    return this.context.clearRect(0, 0, this.element.width, this.element.height);
  };

  return Canvas;
})();

this.Croissant.CanvasStack = (function()
{
  function CanvasStack(canvas_div, size, layers)
  {
    this.add_canvases = __bind(this.add_canvases, this);
    this.canvas_div = $(canvas_div);
    this.canvas_height = parseInt(this.canvas_div.css('height'));
    this.canvas_width = parseInt(this.canvas_div.css('width'));
    if (size[0] && size[1])
    {
      this.size_x = size[0];
      this.size_y = size[1];
    }
    else
    {
      this.size_x = size[0] ? size[0] : size;
      this.size_y = this.size_x;
    }
    if ((this.size_x === this.size_y) && (this.canvas_height === this.canvas_width))
    {
      this.gridsize = this.canvas_width / this.size_x;
    }
    else
    {
      throw 'Non-uniform grid!';
    }
    this.layers = {};
    if (this.canvas_div.css('position') !== 'relative')
    {
      this.canvas_div.css('position', 'relative');
    }
    this.add_canvases(layers);
  }

  CanvasStack.prototype.add_canvases = function(layer_names)
  {
    var layer, layer_name, _i, _len;
    for (_i = 0, _len = layer_names.length; _i < _len; _i++)
    {
      layer_name = layer_names[_i];
      layer = $("<canvas id=" + layer_name + " height=" + this.canvas_height + " width=" + this.canvas_width +
                " z-index=" + (-layer_names.indexOf(layer_name)) + " style='position: absolute; left: 0; top: 0;'>")[0];
      this.canvas_div.append(layer);
      this.layers[layer_name] = new Croissant.Canvas(layer, layer_name, this);
    }
    return false;
  };

  return CanvasStack;
})();

var __bind = function(fn, me)
{
  return function()
  {
    return fn.apply(me, arguments);
  };
};

this.Croissant.Generators.BSP = (function()
{
  function BSP() {}

  BSP.generate = function(level, min_room_size)
  {
    var leaf_array, min_room_size_x, min_room_size_y, root;
    min_room_size_x = 0;
    min_room_size_y = 0;
    if (min_room_size[0] && min_room_size[1])
    {
      min_room_size_x = min_room_size[0];
      min_room_size_y = min_room_size[1];
    }
    else
    {
      min_room_size_x = min_room_size[0] ? min_room_size[0] : min_room_size;
      min_room_size_y = min_room_size_x;
    }
    leaf_array = [];
    root = void 0;
    if ((level.columns > 3 * min_room_size_x + 1) && (level.rows > 3 * min_room_size_y + 1))
    {
      root = new Croissant.Generators.BSPTree(null, [0, level.columns], [0, level.rows],
                                              [min_room_size_x, min_room_size_y], leaf_array);
    }
    return {
      root: root,
      rooms: leaf_array,
      level: level
    };
  };

  return BSP;
})();

this.Croissant.Generators.BSPLeaf = (function()
{
  function BSPLeaf(x, y)
  {
    this.x = x;
    this.y = y;
  }

  return BSPLeaf;
})();

this.Croissant.Generators.BSPTree = (function()
{
  function BSPTree(parent, x, y, min_room_size, leaf_container)
  {
    var split;
    if (leaf_container == null)
    {
      leaf_container = null;
    }
    this.y_size = __bind(this.y_size, this);
    this.x_size = __bind(this.x_size, this);
    this.recurrence_possible = __bind(this.recurrence_possible, this);
    this.parent = parent;
    this.leaf_container = leaf_container ? leaf_container : this.parent.leaf_container;
    this.horizontal = void 0;
    this.x = x;
    this.y = y;
    this.children = [];
    if (this.recurrence_possible(min_room_size))
    {
      if (!this.horizontal)
      {
        split = Math.round(this.x_size() * ((1 / 3) * Math.random() + (1 / 3)));
        this.children.push(new Croissant.Generators.BSPTree(this, [x[0], x[0] + split], [y[0], y[1]], min_room_size));
        this.children.push(new Croissant.Generators.BSPTree(this, [x[0] + split, x[1]], [y[0], y[1]], min_room_size));
      }
      else
      {
        split = Math.round(this.y_size() * ((1 / 3) * Math.random() + (1 / 3)));
        this.children.push(new Croissant.Generators.BSPTree(this, [x[0], x[1]], [y[0], y[0] + split], min_room_size));
        this.children.push(new Croissant.Generators.BSPTree(this, [x[0], x[1]], [y[0] + split, y[1]], min_room_size));
      }
    }
    else
    {
      this.leaf_container.push(new Croissant.Generators.BSPLeaf([x[0], x[1]], [y[0], y[1]]));
    }
  }

  BSPTree.prototype.recurrence_possible = function(room_size)
  {
    if (this.x_size() > 3 * room_size[0] + 1 && this.y_size() > 3 * room_size[1] + 1)
    {
      this.horizontal = Math.random() > .5;
      return true;
    }
    else
    {
      if (this.x_size() > 3 * room_size[0] + 1)
      {
        this.horizontal = false;
        return true;
      }
      else if (this.y_size() > 3 * room_size[1] + 1)
      {
        this.horizontal = true;
        return true;
      }
    }
    return false;
  };

  BSPTree.prototype.x_size = function()
  {
    return Math.abs(this.x[1] - this.x[0]);
  };

  BSPTree.prototype.y_size = function()
  {
    return Math.abs(this.y[1] - this.y[0]);
  };

  return BSPTree;

})();
