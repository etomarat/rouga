var grid_width, grid_height, grid_size;
var context;
var stack, level, rooms;

function shrink_leaf(node)
{    
    x_shrink = Math.round(Math.random() * (node.x_size() / 3));
    y_shrink = Math.round(Math.random() * (node.y_size() / 3));
        
    node.startx = node.x[0] + x_shrink;
    node.starty = node.y[0] + y_shrink;
    node.endx = node.x[0] + node.x_size() - x_shrink;
    node.endy = node.y[0] + node.y_size() - y_shrink;
        
    for (x = node.startx; x < node.endx; ++x)
    {
        for (y = node.starty; y < node.endy; ++y)
        {
            context.fillStyle="#0000FF";
            context.fillRect(x * grid_size, y * grid_size, grid_size, grid_size);
        }
    }
}

function shrink_leafs(node)
{
    if (node.children.length == 0)
    {
        console.log("Found leaf...");
        shrink_leaf(node);
        return;
    }

    console.log("Checking left...");
    if (node.children[0])
    {
        shrink_leafs(node.children[0]);
    }
    
    console.log("Checking right...");
    if (node.children[1])
    {
        shrink_leafs(node.children[1]);
    }
}

function main(argc, argv)
{
    stack = new Croissant.CanvasStack($('#cruassan'), 32, ['rooms']);
    level = new Croissant.Level( 32 );
    rooms = new Croissant.Generators.BSP.generate(level, [5,5]);
    console.log(level);
    console.log(rooms);
    console.log(rooms.root);
    console.log(stack);

    grid_width = rooms.root.x_size();
    grid_height = rooms.root.y_size();
    grid_size = stack.gridsize;
    context = stack.layers['rooms'].context;

    console.log("grid_height: %d, grid_width: %d", grid_width, grid_height);
    console.log("grid_size: %d", grid_size);

    console.log("Shrinking leafs...");
    shrink_leafs(rooms.root);

    console.log("Connecting leafs...");
    connect_leafs(rooms.root);
    
    stack.layers.rooms.draw_grid("#AAAAAA");            
    stack.layers.rooms.draw_boundaries(rooms, "#FF0000");        
}

main(0, "roomgen");